package com.enhops.testscripts;

import java.util.HashMap;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.enhops.framework.core.BaseTest;
import com.enhops.framework.core.ExecutionEngine;
import com.enhops.framework.core.Utilities;
import com.enhops.framework.testdata.TestDataProvider;
import com.enhops.pageobjectfactory.OUA.HomePage;
import com.enhops.pageobjectfactory.OUA.RegisterConfirmPage;
import com.enhops.pageobjectfactory.OUA.RegistrationPage;
import com.enhops.pageobjectfactory.OUA.UserPage;

public class RegisterIntoOUA extends BaseTest {
	
	@Test(dataProvider="TestData", dataProviderClass=TestDataProvider.class)
	public void Register(HashMap<String, String> testData) throws InterruptedException
	{
		Utilities.delayFor(4000);
		HomePage oua_Home = PageFactory.initElements(ExecutionEngine.getSeleniumWebDriver(),HomePage.class);
		oua_Home.navigateToRegistration();
		
		String firstName = testData.get("FirstName");
		String familyName = testData.get("FamilyName");
		String emailID = testData.get("EmailId");
		String userName = testData.get("UserName");
				
		RegistrationPage reg = PageFactory.initElements(ExecutionEngine.getSeleniumWebDriver(), RegistrationPage.class);
		reg.registerinoua(firstName,familyName,emailID,userName);
		
		Utilities.delayFor(4000);
		
		RegisterConfirmPage regconf = PageFactory.initElements(ExecutionEngine.getSeleniumWebDriver(),RegisterConfirmPage.class);
		regconf.confirmRegister();
		
		Utilities.delayFor(1000);
		
		UserPage userPage = PageFactory.initElements(ExecutionEngine.getSeleniumWebDriver(), UserPage.class);
		userPage.selectCourseOrUnit();
		
		}
	
}