package com.enhops.framework.core;

import java.util.concurrent.TimeUnit;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.enhops.framework.testreports.ReportManager;
public class BaseTest {
	
	
	public static void ClassInit()
	{
		
	}
	
	public static void ClassCleanup()
	{
		
	}
	
	@BeforeMethod
	public void testInit(ITestContext context)
	{
		ReportManager.initalizeTestCase(context.getName(), context.getName());
	}
	
	@BeforeSuite
	public void SuiteInit(ITestContext context) 
	{
		ReportManager.intializeTestReport(context.getSuite().getName());
		ExecutionEngine.CreateSeleniumWebDriver();
		ExecutionEngine.getSeleniumWebDriver().navigate().to(Configuration.getProperty("URL"));
		//ExecutionEngine.getSeleniumWebDriver().switchTo().window("");
		ExecutionEngine.getSeleniumWebDriver().manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		Utilities.delayFor(3000);
	}
	
	@AfterSuite
	public void SuiteCleanup(ITestContext context) 
	{
		ExecutionEngine.getSeleniumWebDriver().close();
		ReportManager.SaveTestReport();
	}
	@AfterMethod
	public void testCleanup()
	{
		ReportManager.ClostTestCaseReport();
	}

}