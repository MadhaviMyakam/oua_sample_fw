package com.enhops.pageobjectfactory.OUA;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.enhops.framework.core.ExecutionEngine;
import com.enhops.framework.core.Utilities;
import com.enhops.framework.testreports.ReportManager;


public class RegistrationPage 
{
	@FindBy(id = "firstName")
	public WebElement txt_firstname;
	
	@FindBy(id = "familyName")
	public WebElement txt_familyName;
	
	@FindBy(id = "email")
	public WebElement txt_email;
	
	@FindBy(xpath ="//*[@id='day']")
	public WebElement select_day;
	
	@FindBy(id = "month")
	public WebElement select_month;
	
	@FindBy(id = "year")
	public WebElement select_year;
	
	@FindBy(id = "username")
	public WebElement txt_username;
	
	@FindBy(id = "password")
	public WebElement txt_password;
	
	@FindBy(id = "confirmPassword")
	public WebElement txt_rePassword;
	
	@FindBy(id = "secretQuestionKey")
	public WebElement select_securityQ;
	
	@FindBy(id = "secretAnswer")
	public WebElement txt_securityAnswer;
	
	@FindBy(id = "publicProfile2")
	public WebElement radio_privateprofile;
	
	@FindBy(id = "publicProfile1")
	public WebElement radio_publicprofile;
	
	@FindBy(id = "receiveEmail1")
	public WebElement cb_ReceiveEmail;
	
	@FindBy(xpath = "//*[@id='createLoginBasicForm']/div[1]/input")
	public WebElement btn_accept;
	
	public void registerinoua(String firstName,String familyName,String emailID,
			String userName) throws InterruptedException {
		if(txt_firstname.isDisplayed())
		{
			ReportManager.LogSuccess("Registration Flow", "Registration page", "Register page should be displayed", "Register page displayed", false);
			txt_firstname.sendKeys(firstName);
			
			txt_familyName.sendKeys(familyName);
			
			txt_email.sendKeys(emailID);
			
			txt_email.sendKeys(Keys.TAB);
			
			select_day.sendKeys(Keys.ARROW_DOWN);
			
			Select day = new Select(select_day);
			select_day.click();
			day.selectByVisibleText("05");
			System.out.println("Month SELECT*****"+day.getFirstSelectedOption().getText());
			
			select_month.sendKeys(Keys.ARROW_DOWN);
			Select month = new Select(select_month);
			month.selectByVisibleText("Mar");
			System.out.println("Month SELECT*****"+month.getFirstSelectedOption().getText());
			
			select_month.sendKeys(Keys.TAB);
			
			select_year.sendKeys(Keys.ARROW_DOWN);
			Select year = new Select(select_year);
			year.selectByValue("1990");
			
			txt_username.sendKeys(userName);
			
			txt_password.sendKeys("Enhops123");
			
			txt_rePassword.sendKeys("Enhops123");
			
			select_securityQ.sendKeys(Keys.ARROW_DOWN);
			/*Select secretQ = new Select(select_securityQ);
			secretQ.selectByValue("Favourite TV show");*/
			
			select_securityQ.sendKeys(Keys.TAB);
			
			Utilities.delayFor(1000);
			txt_securityAnswer.sendKeys("Enhops123");
			
			radio_publicprofile.click();
			
			cb_ReceiveEmail.click();
			Utilities.delayFor(1000);
			btn_accept.click();
		}
		else
		{
			ReportManager.LogFailure("Registration Flow","Registration page" , "Register page should be displayed", "Register page is not displayed", true);
		}
		
			
	
		}
		
			
	}

