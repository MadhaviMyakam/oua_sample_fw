package com.enhops.pageobjectfactory.OUA;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.enhops.framework.testreports.ReportManager;

public class HomePage 
{
	
	@FindBy(xpath = "//a[contains(text(),'Register')]")
	public WebElement lnk_Register;
	
	public void navigateToRegistration() {
		if(lnk_Register.isDisplayed())
		{
			
			ReportManager.LogSuccess("HomePage", "OUA Home page", "Home page should be displayed", "Home page displayed", false);
			lnk_Register.click();
		}
		else
		{
			ReportManager.LogFailure("HomePage","OUA Home page" , "Home page should be displayed", "Home page is not displayed", true);
		}
		
			
	}
	
	
	
	
}
