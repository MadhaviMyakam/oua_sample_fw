package com.enhops.pageobjectfactory.OUA;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.enhops.framework.testreports.ReportManager;

public class RegisterConfirmPage 
{
	@FindBy(name = "confirm")
	public WebElement btn_continue;

	public void confirmRegister()
	{
		if(btn_continue.isDisplayed())
		{
			btn_continue.click();
			ReportManager.LogSuccess("Registration Flow", "User Registation is sucessfull", "Registration is sucessfull", "Registration  is sucessfull", false);
		}
		else
		{
			ReportManager.LogFailure("Registration Flow", "User Registation is sucessfull","Registration is sucessfull" ,"Registration is not sucessfull" , true);
		}
	}
}
