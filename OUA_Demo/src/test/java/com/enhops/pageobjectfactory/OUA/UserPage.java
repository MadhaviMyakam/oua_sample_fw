package com.enhops.pageobjectfactory.OUA;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.enhops.framework.core.Utilities;
import com.enhops.framework.testreports.ReportManager;

public class UserPage 
{

	@FindBy(xpath = "//a[contains(text(),'Browse courses & units')]")
	public WebElement btn_browseCourse;

	@FindBy(xpath = "/html/body/header/div[2]/div/div[2]/ul/li[3]/a")
	WebElement lnk_profile;
	
	@FindBy(xpath = "/html/body/header/div[2]/div/div[2]/ul/li[3]/ul/li[4]/a")
	WebElement lnk_signout;
	
	@FindBy(id ="course-unit-search-box")
	public WebElement txt_CourseSreachbox;
	
	@FindBy(xpath ="//a[contains(text(),'courses/it')]")
	public WebElement lnk_courseOrUnit;
	
	@FindBy(xpath ="//*[@id='content']/section[1]/div/div[1]/div[2]")
	public WebElement noticeBar;
	
	@FindBy(xpath = "//a[contains(text(),'View 2016 course details.')]")
	public WebElement lnk_2016Link;
	
	@FindBy(xpath = "//a[contains(text(),'View 2017 course details.')]")
	public WebElement lnk_2017Link;
	
	@FindBy(name = "study-cart")
	public WebElement btn_addtocart;
	
	@FindBy(xpath = "//a[contains(text(),'Go to Study Cart & Wishlist')]")
	public WebElement lnk_gotowishlist;

	@FindBy(xpath = "//span[contains(text(),'Take action')]")
	public WebElement lnk_takeaction;

	@FindBy(xpath = "//button[@value='I acknowledge']")
	public WebElement btn_Iacknowledge;

	@FindBy(id = "citizenshipStatus")
	public WebElement select_citizenshp;

	@FindBy(id ="studyCartEnrol")
	public WebElement btn_enrolNow;


	public void selectCourseOrUnit()
	{
		if(btn_browseCourse.isDisplayed())
		{
			ReportManager.LogSuccess("Registration Flow", "User page is displayed", "User page  should be displayed", "User page is displayed", false);
			btn_browseCourse.click();
			Utilities.delayFor(3000);
			
			lnk_profile.click();
			
			
			if(lnk_signout.isDisplayed())
			{
				lnk_signout.click();
				ReportManager.LogSuccess("Logout", "Click on logout link ", "Logout link is clicked", "logout is sucessfull", false);
			}
			
			
			/*if(txt_CourseSreachbox.isDisplayed())
			{
				txt_CourseSreachbox.sendKeys("cpt140");
				
				lnk_courseOrUnit.click();
				
				if(noticeBar.isDisplayed())
				{
					if(lnk_2016Link.isDisplayed())
					{
						lnk_2016Link.click();
					}
					
					if(lnk_2017Link.isDisplayed())
					{
						lnk_2017Link.click();
					}
					
					btn_addtocart.click();
					
					lnk_gotowishlist.click();
				}
			}*/
		}
		else
		{
			ReportManager.LogFailure ("4", "User page is displayed", "User page  should be displayed", "User page is not displayed", true);
		}
			
			
		}
	}


